-- Christina
-- CS440
-- MP3 Higher Order Functions

module HOF
( mymap
, flatmap
, myfoldr
, myfoldl
, myzipwith
, exists
, forall
, myfilter
, inclist
, doublelist
, sumlist
, prodlist
, sumpairs
, intersect
, nats
, fib
) where


mymap :: (t -> a) -> [t] -> [a]
mymap f [] = []
mymap f (x:xs) = f x : map f xs


flatmap :: (t -> [a]) -> [t] -> [a]
flatmap f [] = []
flatmap f (x:xs) = f x ++ flatmap f xs


myfoldr :: (t1 -> t -> t) -> t -> [t1] -> t
myfoldr f z [] = z
myfoldr f z (x:xs) = f x (myfoldr f z xs)


myfoldl :: (t -> t1 -> t) -> t -> [t1] -> t
myfoldl f z [] = z
myfoldl f z (x:xs) = myfoldl f (f z x) xs


myzipwith :: (a -> b -> c) -> [a] -> [b] -> [c]
myzipwith f _ [] = []
myzipwith f [] _ = []
myzipwith f (x:xs) (y:ys) = f x y : myzipwith f xs ys


exists :: (t -> Bool) -> [t] -> Bool
exists f [] = False
exists f (x:xs)
	| f x == True	= True
	| otherwise	= exists f xs


forall :: (t -> Bool) -> [t] -> Bool
forall f xx = eval f xx False
	where eval f [] b = b
	      eval f (x:xs) b
		| f x == True = eval f xs True
		| otherwise = False


myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f [] = []
myfilter f (x:xs)
	| f x == True	= x : myfilter f xs
	| otherwise	= myfilter f xs


inclist :: (Num a) => [a] -> [a]
inclist xx = mymap (+1) xx


doublelist :: [Integer] -> [Integer]
doublelist xx = map (*2) xx


sumlist :: [Integer] -> Integer
sumlist xx = myfoldr (+) 0 xx


prodlist :: [Integer] -> Integer
prodlist xx = myfoldr (*) 1 xx


sumpairs :: (Num a) => [a] -> [a] -> [a]
sumpairs xx yy = myzipwith (+) xx yy


intersect :: (Eq a) => [a] -> [a] -> [a]
intersect xx yy = myfilter (`elem` xx) yy


nats :: [Integer]
nats = [1,2..]


fib :: [Integer]
fib = 1 : 1 : myzipwith (+) fib (tail fib)
