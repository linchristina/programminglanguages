-- Christina Lin
-- CS440
-- MP2 Disjoint Types

module Types
( Snoc(..)
, BST(..)
) where

data Snoc a = Snoc (Snoc a) a | Lin deriving Show
data BST a = Branch a (BST a) (BST a) | Empty deriving Show


shead :: Snoc a -> a
shead (Snoc _ x) = x


stail :: Snoc a -> Snoc a
stail (Snoc xs _) = xs


list2snoc :: [a] -> Snoc a
list2snoc [] = Lin
list2snoc (x:xs) = Snoc (list2snoc xs) x


add :: (Ord a) => a -> BST a -> BST a
add x Empty = Branch x Empty Empty
add x (Branch a left right)
	| x == a	= Branch x left right
	| x < a		= Branch a (add x left) right
	| x > a		= Branch a left (add x right)


del :: (Ord a) => a -> BST a -> BST a
del _ Empty = Empty
del x (Branch a left right)
	| x == a	= rebuild left right
	| x < a		= Branch a (del x left) right
	| otherwise	= Branch a left (del x right)

rebuild :: BST a -> BST a -> BST a
rebuild Empty Empty = Empty
rebuild Empty x = x
rebuild x Empty = x
rebuild x (Branch a left right) = Branch a (rebuild x left) right

-- This is an interesting algorithm.  I think the predecessor copy version we use in CS 331
-- will be more efficient as far as the tree structure, but this is fine.

find :: (Ord a) => a -> BST a -> Maybe a
find x Empty = Nothing
find x (Branch a left right)
	| x == a	= Just x
	| x < a		= find x left
	| x > a		= find x right
	| otherwise	= Nothing
