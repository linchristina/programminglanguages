% Christina Lin
% CS440
% Prolog MP

connected(alligatorpond,backdoor,2).
connected(backdoor,exit,1).
connected(bedroom,library,5).
connected(billiardroom,greenhouse,2).
connected(coatroom,servantroom,2).
connected(diningroom,alligatorpond,2).
connected(diningroom,kitchen,4).
connected(diningroom,library,7).
connected(entrance,mainhall,2).
connected(greenhouse,backdoor,5).
connected(kitchen,billiardroom,5).
connected(kitchen,greenhouse,1).
connected(library,billiardroom,3).
connected(mainhall,bedroom,4).
connected(mainhall,coatroom,3).
connected(mainhall,diningroom,3).
connected(mainhall,library,6).
connected(servantroom,alligatorpond,2).
connected(servantroom,diningroom,3).


% finds all valid paths
pathfrom(X,Y,[X,Y]) :- connected(X,Y,_).
pathfrom(X,Y,[X|R]) :- connected(X,L,_),
		       pathfrom(L,Y,R).


% cost of a path
costof([X,Y],C) :- connected(X,Y,C).
costof([X,Y|L],C) :- connected(X,Y,A),
		     costof([Y|L],B),
		     C is A + B.

% list of costs corresponding to list of paths
costoflist([[X|L]],[C]) :- costof([X|L],C).
costoflist([[X|L]|R],[H|T]) :- costof([X|L],H),
			       costoflist(R,T).


% find X, minimum element of the list
minlist(X,[X]).
minlist(X,[H,Y|T]) :- H =< Y,
		      minlist(X,[H|T]).
minlist(X,[H,Y|T]) :- H > Y,
		      minlist(X,[Y|T]).

mincost(P,C,[X|Y],[H|T]) :- member2(P,C,[X|Y],[H|T]),
			    minlist(C,[H|T]).
mincost(P,C,[[X|Y]|Z],[H|T]) :- member2(P,C,[[X|Y]|Z],[H|T]),
			        minlist(C,[H|T]),
			        mincost(P,C,Z,[H|T]).

member2(A,B,[A|_],[B|_]).
member2(A,B,[_|X],[_|Y]) :- member2(A,B,X,Y).


% finds the shortest valid path
findpath(A,B,P,C) :- findall(X,pathfrom(A,B,X),L),	% list of paths
		     costoflist(L,Costs),		% list of costs
		     mincost(P,C,L,Costs).


% avoids alligator pond and goes through either
% servant quarters or billiard room
goodpath(L) :- member(alligatorpond,L), !, fail.
goodpath(L) :- member(servantroom,L), !;  member(billiardroom,L), !.


% finds all valid paths and eliminates those that have alligatorpond
% or those that don't have servantroom or billiardroom
goodpathfrom(A,B,P) :- pathfrom(A,B,P), goodpath(P).

findgoodpath(A,B,P,C) :- findall(X,goodpathfrom(A,B,X),L),
			 costoflist(L,Costs),
			 mincost(P,C,L,Costs).
