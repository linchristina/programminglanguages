-- Christina Lin
-- CS440
-- MP1 Recursion

module Recursion
( fact,
  calc,
  mytake,
  mydrop,
  rev,
  app,
  inclist,
  sumlist,
  myzip,
  addpairs,
  pow
) where


-- a recursive function fact n that returns n!
fact :: (Eq t, Num t) => t -> t
fact 0 = 1
fact n = n * fact (n-1)


-- count how many times calcstep must be run in order to yield the number 1
-- calcstep(n) = n/2	if n is even
-- calcstep(n) = n*3+1	if n is odd
calc n = calcstep n 0
calcstep n i
	| n==0 = i
	| n==1 = i
	| odd n = calcstep (n*3+1) (i+1)
	| otherwise = calcstep (n `div` 2) (i+1)


-- mytake n xx returns first n elements of xx
mytake :: (Eq t, Num t) => t -> [a] -> [a]
mytake 0 (x:xx) = []
mytake n [] = []
mytake n (x:xx) = x : mytake (n-1) xx


-- mydrop n xx returns all but the first n elements of list xx
mydrop :: (Eq t, Num t) => t -> [t1] -> [t1]
mydrop 0 (x:xx) = x:xx
mydrop n [] = []
mydrop n (x:xx) = mydrop (n-1) xx


-- rev xx returns the reverse of list xx
rev :: [t] -> [t]
rev xx = aux xx []
   where aux [] a = a
	 aux (x:xs) a = aux xs (x:a)


-- app xx yy appends two lists together
app :: [t] -> [t] -> [t]
app [][] = []
app [](y:yy) = (y:yy)
app (x:xx)[] = (x:xx)
app (x:xx)(y:yy) = (x:xx) ++ (y:yy)


-- inclist takes a list and returns it incremented by one
inclist :: (Num a) => [a] -> [a]
inclist = map (+1)


-- sumlist takes a list and returns the sum
sumlist :: (Num t) => [t] -> t
sumlist = foldr (+) 0


-- zip takes two lists and returns the list with the corresponding matches in pairs
myzip :: [t] -> [t1] -> [(t, t1)]
myzip [] _ = []
myzip _ [] = []
myzip (x:xs) (y:ys) = (x,y) : myzip xs ys


-- using the zip function from above, addpairs takes two lists and returns the list with the corresponding values added
addpairs :: (Num a) => [a] -> [a] -> [a]
addpairs xx yy = [a+b | (a,b) <- myzip xx yy]


-- powerset xx returns the powerset of xx
pow :: [a] -> [[a]]
pow [] = [[]]
pow (x:xs) = map (x:) (pow xs) ++ pow xs
